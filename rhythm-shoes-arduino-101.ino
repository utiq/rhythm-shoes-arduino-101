/*
   This sketch demonstrates how the rhythm shoes would work using
   the Intel(R) Curie(TM) module.
*/

#include "CurieIMU.h"
#include <CurieBLE.h>

BLEService shoeService("19B10010-E8F2-537E-4F6C-D104768A1214");

BLEUnsignedCharCharacteristic vibrateCharacteristic("19B10011-E8F2-537E-4F6C-D104768A1214", BLERead | BLEWrite);
BLECharCharacteristic tapCharacteristic("19B10012-E8F2-537E-4F6C-D104768A1214", BLERead | BLENotify); // allows remote device to get notifications

// Pins configuration
const int FRONT = 8;
const int RIGHT = 9;
const int BACK = 10;
const int LEFT = 11;
const int TOP = 12;

void setup() {
  Serial.begin(9600);
  while(!Serial);

  /****************************** DETECT TAP CONFIG ************************************/
  CurieIMU.begin();
  CurieIMU.attachInterrupt(eventCallback);

  CurieIMU.setAccelerometerRange(4);
  CurieIMU.setDetectionThreshold(CURIE_IMU_TAP, 750); // (750mg)
  CurieIMU.interrupts(CURIE_IMU_TAP);

  Serial.println("IMU initialization complete, waiting for events...");

  /**************************** PINS SETUP **********************************************/
  pinMode(FRONT, OUTPUT);
  pinMode(RIGHT, OUTPUT);
  pinMode(BACK, OUTPUT);
  pinMode(LEFT, OUTPUT);
  pinMode(TOP, OUTPUT);

  /***************************** BLE SETUP ***********************************************/
  BLE.begin();
  BLE.setLocalName("RhythmShoe");
  BLE.setAdvertisedService(shoeService);
  shoeService.addCharacteristic(vibrateCharacteristic);
  shoeService.addCharacteristic(tapCharacteristic);
  BLE.addService(shoeService);
  vibrateCharacteristic.setValue(0);
  tapCharacteristic.setValue(0);
  BLE.advertise();
  Serial.println("rhythm shoe active, waiting for connection...");
  
}

void loop() {

  BLEDevice central = BLE.central();

  if (central) {
    Serial.print("Connected to central: ");
    Serial.println(central.address());

    while (central.connected()) {
      if (vibrateCharacteristic.written()) {
        Serial.println(vibrateCharacteristic.value());
        vibrate(vibrateCharacteristic.value());
      }
    }

    Serial.print(F("Disconnected from central: "));
    Serial.println(central.address());
  }
}

static void eventCallback()
{
  if (CurieIMU.getInterruptStatus(CURIE_IMU_TAP)) {
    // To be more precise, we could detect the way how the user is stepping calculating the X,Y,Z-axis position.
    // For example, in salsa you have to step with the sole, but in bachata you have to tap with the forefoot
    // For practical purposes, we will just count if the user has stepped.
    Serial.println("Dancer has stepped");
    tapCharacteristic.setValue(1);
  }
}

void vibrate(int motor) {
  int pin = 0;
  
  switch(motor) {
    case 49:
      pin = FRONT;
      break;
    case 50:
      pin = RIGHT;
      break;
    case 51:
      pin = BACK;
      break;
    case 52:
      pin = LEFT;
      break;
    case 53:
      pin = TOP;
      break;
    default:
      Serial.println("No motor");
      break;
  }

  digitalWrite(pin, HIGH);
  delay(200);
  digitalWrite(pin, LOW);
}
